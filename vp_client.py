#! /usr/bin/python -u
# -*- coding: utf-8 -*-
# Recommend: use pyenv with Python 2.x 

import os
import subprocess
import sys
import time
from datetime import datetime
try:
    import json
    import serial
    import gevent
    from pprint import pprint
    from watchdog.observers import Observer
    from watchdog.events import PatternMatchingEventHandler
    from watchdog.events import LoggingEventHandler
    from PyCRC.CRCCCITT import CRCCCITT
    import find_serial
except ImportError:
    raise ImportError('Please, check libraries dependencies')


# Version
# --------------------------
VERSION="1.6.2"
AUTHOR="Carlos Almeida Jr"
# RELEASE="20/oct/2018"

# Changelog
# --------------------------
# 06/02/2020 HOTFIX: TEF started to fail after inform protocol. Added _PROTOCOL_RECEIVED 
#                    as a way to guarantee payment confirmation
# 06/05/2019 HOTFIX: Fixed recurrenting order throwing after vendTef lose syncronism
# 20/05/2019 FUNCTION: Generates transaction log
# 20/05/2019 HOTFIX: Avoid repetead RESET operation (due to serial cable issues)
# 21/05/2019 HOTFIX: Ignore #_NO_ANSWER_LIMIT limit of "no answers" from VendTEF
#                    No answer impose a really serious problem in VendTEF communication.
#                    Some machines are affected with this intermitent problem that is
#                    very difficult to be diagnosed. Ignore some no answers shows that
#                    communication becomes more stable than before, but this approach
#                    must be reviewed.
# 20/10/2020 ADDED:  Method to find serial port by VM settings.conf

# GLOBAL VARS
# -------------------------- 

DEBUG = False

__STATE = "init"
_ORDER_TYPE = ""
_VERIFY_COUNTER = 0
_VERIFY_LIMIT  = 10
_NO_ANSWER_LIMIT = 4
_RESET = False

_term_available = ""
_label_order = ""
_product = {}
_protocol = []
_PROTOCOL_RECEIVED = False
_payment_status = ""
_releaseAction = "none"
_last_packet = ""
_no_answer_count = 0

# Notify
_notifyDir = "/var/broker/"
_actionDir = "/var/broker/action" 

# Transaction log
_fileHandler = ""
_log = "/opt/broker/transaction/"
_logFilename = ""
_lastMessage = "" # To avoid repetead messages


# SERIAL PARAMS
# --------------------------

_device = find_serial.find_serial()
_serialHandler = None # TBD after instance
_baudrate = 9600
_timeout  = 0.1
_delay_write = 0.1
# GOOD SERIAL CABLES SUPPORT 20 ms or 0.2 - WORST CASE: 0.45
_comm_period = 0.45 # max: 50 ms between VM and Terminal operations
_sleepInterval = 0.2

# VENDPAGO COMMANDS
# --------------------------

__STX      = "\x02"
__ETX      = "\x03"
__ACK      = "\x06"
__NACK     = "\x15"
__RESET    = "\x10"
__STATUS   = "\x53"
__ENQ      = "\x05"
__OUTOFORDER = "\x56"

__STATUS_3   = "\x33"  # IDLE
__STATUS_4   = "\x34"  # SINGLE ORDER
__STATUS_5   = "\x35"  # MULTI ORDER

__VERIFY_TERM     = "\x57"
__TERM_STATUS_OK  = "\x31"
__TERM_STATUS_NOK = "\x30"

__MASTER_INIT   = "\x11\x00"
__SLAVE_INIT    = "\x11\x01"
__ENABLE_SLAVE  = "\x14\x31"
__DISABLE_SLAVE = "\x14\x30"   

__SINGLE_ORDER  = "\x54"
__MULT_ORDER    = "\x4d"
__MO_INIT       = "\x30"
__MO_END        = "\x31"

__APPROVED_PAYMENT  = "\x31" 
__REFUSED_PAYMENT   = "\x30"

__PROTOCOL          ="\x49"

__RELEASE           = "\x52"
__RELEASE_CONFIRM   = "\x31" 
__RELEASE_REFUSE    = "\x30"

__INACTIVED_DEVICE  = "\x31" 
__DISABLED_DEVICE   = "\x32"

__COMM_STATUS = {
                    __STX : "STX",
                    __ETX : "ETX",
                    __ACK : "ACK",
                    __NACK : "NACK",
                    __STATUS : "STATUS",
                    __PROTOCOL: "PROTOCOL",
                    __ENQ : "ENQUIRY",
                    __OUTOFORDER : "OUT OF ORDER"
                }

__DEVICE_STATUS =   {
                    __INACTIVED_DEVICE : "DEVICE INACTIVE", 
                    __DISABLED_DEVICE : "DEVICE DISABLED",
                    __STATUS_3 : "DEVICE ENABLED"
                }

__TRANSACTION_STATUS = { 
                    __APPROVED_PAYMENT  : "PAYMENT APPROVED",
                    __REFUSED_PAYMENT   : "PAYMENT REFUSED" 
                }

__ORDER_STATUS = {
                    __STATUS_4 : "SINGLE ORDER STATUS",
                    __STATUS_5 : "MULTI ORDER STATUS"
                }

__NOTIFY = {
                "transaction": "",
                "status": "",
                "protocol": ""
            }

# Adding  # ORDER_STATUS : {SO,MO}_STATUS
__DEVICE_STATUS.update(__ORDER_STATUS)
__TRANSACTION_STATUS.update(__ORDER_STATUS)

# FINITE STATE MACHINE
# --------------------------

def FSM():
    global __STATE

    while True:
        while __STATE == "init":
            stateInit()
            gevent.sleep(_sleepInterval)
        while __STATE == "idle":
            stateIdle()
            gevent.sleep(_sleepInterval)
        while __STATE == "verifying-terminal":
            stateVerifyingTerminal()
            gevent.sleep(_sleepInterval)
        while __STATE == "single-order":
            stateSingleOrder()
            gevent.sleep(_sleepInterval)
        while __STATE == "multi-order":
            stateMultiOrder()
            gevent.sleep(_sleepInterval)
        while __STATE == "protocol":
            stateProtocol()
            gevent.sleep(_sleepInterval)
        while __STATE == "exec-order":
            stateExecOrder()
            gevent.sleep(_sleepInterval)
        while __STATE == "release":
            stateRelease()
            gevent.sleep(_sleepInterval)
        
        gevent.sleep(_sleepInterval)


# STATES DEFINITION
def stateInit():
    global __STATE, _ORDER_TYPE, _RESET, _PROTOCOL_RECEIVED
    _ORDER_TYPE = ""
    _PROTOCOL_RECEIVED = False
    print("\n\t[ INIT ]")
    if sendReset() or _RESET: 
        if sendInitializeDevice():
            _RESET = False
            if sendEnableDevice():
                notify("status", __STATUS_3) # ENABLED
                __STATE = "idle"

def stateIdle():
    global __STATE, _VERIFY_LIMIT, _VERIFY_COUNTER, _ORDER_TYPE, _last_packet, _releaseAction, _no_answer_count, _PROTOCOL_RECEIVED
    _no_answer_count = 0
    _PROTOCOL_RECEIVED = False
    print("\n\t[ IDLE ]")

    if sendENQ({__STATUS_3,__ACK}): #NOTE1: this can returns ALSO AN ACK - It is differently of the protocol's specs
        sendACK()
        notify("status", __DEVICE_STATUS[__STATUS_3])
    else: 
        print("\t[ IDLE ] OUT OF ORDER - RESETING...")
        __STATE = "init"
        
    if _ORDER_TYPE in { "SINGLE", "MULTI" }:
        _releaseAction = "none"
        _VERIFY_COUNTER = _VERIFY_LIMIT 
        genLog("\t[ IDLE ] STARTING TRANSACTION: %s"%_ORDER_TYPE)
        __STATE = "verifying-terminal" 
    else:
        closeLogFile()

def stateVerifyingTerminal():
    global __STATE, _ORDER_TYPE, _VERIFY_COUNTER, _VERIFY_LIMIT
    print("\n\t[ VERIFYING TERMINAL ]")

    if sendVerifyTerminal():
        sendACK()
        if _ORDER_TYPE == "SINGLE":
            __STATE = "single-order"
        else:
            __STATE = "multi-order"

    if _VERIFY_COUNTER == 0:
        genLog("\t[ VERIFYING TERMINAL ] AFTER %d tries, terminal stills unavailable. Cancelling order..."%_VERIFY_LIMIT)
        __STATE = "init"

    _VERIFY_COUNTER = _VERIFY_COUNTER - 1

def stateSingleOrder():
    global __STATE, _payment_status, _product
    genLog("\n\t[ SINGLE ORDER ]")

    if sendOrder("SingleOrder", _product[0]['product'], _product[0]['price']):
        if sendENQ(__STATUS_4):
            sendACK()
            notify("status", __DEVICE_STATUS[__STATUS_4])
            notify("transaction", "")
            _payment_status = ""
            __STATE = "exec-order"
        elif _payment_status == "refused":
            genLog("\t[ SINGLE ORDER ] OUT OF ORDER PACKET - RESETING...")
            __STATE = "init"          
    else:
        genLog("\t[ SINGLE ORDER ] OUT OF ORDER PACKET - RESETING...")
        __STATE = "init"

def stateMultiOrder():
    global __STATE, _payment_status, _product, _NO_ANSWER_LIMIT, _no_answer_count
    print("\n\t[ MULTI ORDER ]")

    if sendInitMultiOrder():
        if sendENQ(__STATUS_5):
            sendACK()
            notify("status", __DEVICE_STATUS[__STATUS_5])

            for p in _product:
                if(sendOrder("MultiOrder", _product[p]['product'], _product[p]['price'])):
                    if sendENQ(__STATUS_5):
                        sendACK()
                    elif _payment_status == "refused":
                        genLog("\t[ SINGLE ORDER ] OUT OF ORDER PACKET - RESETING...")
                        __STATE = "init"    
                    else:
                        if _no_answer_count >= _NO_ANSWER_LIMIT:                            
                            genLog("\t[ MULTI-ORDER ] FAILED TO ADD PROD: %s"%_product[p]['product'])
                            __STATE = "init"
                        else:
                            sendACK() 
                            _no_answer_count = _no_answer_count + 1 
            
            if sendENQ(__STATUS_5):
                sendACK()
                if sendEndMultiOrder():
                    _payment_status = ""
                    __STATE = "exec-order"
                else:
                    if _no_answer_count >= _NO_ANSWER_LIMIT:  
                        genLog("\t[ MULTI-ORDER ] FAILED TO END")
                        __STATE = "init"
                    else:
                        sendACK()
                        _no_answer_count = _no_answer_count + 1 
            else:
                genLog("\t[ MULTI-ORDER ] FAILED TO RECEIVE (STATUS 5)")
                __STATE = "init"
    else:
        genLog("\t[ MULTI-ORDER ] FAILED TO START PROCESS")
        __STATE = "init"

def stateExecOrder():
    global  __STATE, _ORDER_TYPE, _payment_status, _releaseAction, _NO_ANSWER_LIMIT, _no_answer_count
    genLog("\n\t[ EXECUTING ORDER ]")   
    
    if _ORDER_TYPE == "SINGLE":
        if sendENQ({__STATUS_4, __APPROVED_PAYMENT, __REFUSED_PAYMENT}):
            sendACK()
        else:
            if _no_answer_count >= _NO_ANSWER_LIMIT:
                genLog("\t[ EXEC SINGLE-ORDER ] OUT OF ORDER PACKET - RESETING...")
                __STATE = "init"
            else:
                sendACK() 
                _no_answer_count = _no_answer_count + 1
                
    elif _ORDER_TYPE == "MULTI":
        if sendENQ({__STATUS_5, __APPROVED_PAYMENT, __REFUSED_PAYMENT}):
            sendACK()
            time.sleep(0.01) # note 4: daemon gets OUT OF ORDER without this
        else:
            if _no_answer_count >= _NO_ANSWER_LIMIT:
                genLog("\t[ EXEC MULTI-ORDER ] OUT OF ORDER PACKET - RESETING...")
                __STATE = "init"
            else:
                sendACK() 
                _no_answer_count = _no_answer_count + 1

    if _payment_status == "approved":
         _ORDER_TYPE = ""
         _no_answer_count = 0
         __STATE = "protocol"
    elif _payment_status == "refused":
        notify("transaction", "PAYMENT REFUSED")
        _ORDER_TYPE = ""
        __STATE = "idle"

    if _releaseAction in { "CONFIRM", "CANCEL" }:
        _ORDER_TYPE = ""
        _no_answer_count = 0
        __STATE = "release"

def stateProtocol():
    global  __STATE, _releaseAction, _NO_ANSWER_LIMIT, _no_answer_count, _PROTOCOL_RECEIVED
    genLog("\n\t[ REQUESTING PROTOCOL ]")

    if sendProtocolRequest():
        if sendENQ({__STATUS_4, __STATUS_5}) or _PROTOCOL_RECEIVED:
            sendACK() 
            notify("transaction", "PAYMENT APPROVED")
            # BYPASS - this is informed by application but now is hard-coded
            # Must be refactored in application in new core sale
            # After that, remove next line
            _no_answer_count = 0
            _releaseAction = "CONFIRM"
            __STATE = "release"
    else:
        if _no_answer_count >= _NO_ANSWER_LIMIT:
            genLog("\t[ PROTOCOL ] OUT OF ORDER PACKET - RESETING...")
            __STATE = "init" 
        else:
            sendACK() 
            _no_answer_count = _no_answer_count + 1

def stateRelease():
    global  __STATE, _releaseAction, _NO_ANSWER_LIMIT, _no_answer_count
    genLog("\n\t[ RELEASING PRODUCT ]")

    if sendGrantOrder(_releaseAction):
        _releaseAction = "none"
        _ORDER_TYPE = ""
        __STATE = "idle"
    else:
        if _no_answer_count >= _NO_ANSWER_LIMIT:
            genLog("\t[ RELEASE ] OUT OF ORDER PACKET - RESETING...")
            __STATE = "init"
        else:
            _no_answer_count = _no_answer_count + 1


# HELPERS
def flushSerial():
    _serialHandler.reset_input_buffer()
    _serialHandler.reset_output_buffer()
    _serialHandler.flush()

def crc16ccitt(data):
    result = CRCCCITT().calculate(data)
    return [chr(result>>8),chr(result & 0XFF)] 

def buildPacket(command):
    CRC = crc16ccitt(command)
    return __STX + command + __ETX + CRC[1] + CRC[0]

def convertToCents(price): #price is a float number 999.99 (Pay attention in this limit!!)
    genLog("\t[ CONVERTION ] TYPE: %s VALUE: %s"%(type(price),price))
    price = int(round(float(price)*100))
    return str(price).zfill(5) # Expected format 00000

def notify(notify, value):
    global _notifyDir
    if __NOTIFY[notify] != value:
        __NOTIFY[notify] = value
        uri = _notifyDir + "/" + notify + ".json"
        f = open(uri, 'w')
        f.write("{ \n\t\"notify\" : \"" + notify + "\",\n\t\"value\" : \"" +  value + "\"\n\r}\n")
        f.close()

        print("\t[ NOTIFY ] RECORDING - notify: %s, value: %s" % (notify,value))

# COMMANDS FROM FILE WATCHER - NOTIFY
def startOrder(type, product):
    global _product, _ORDER_TYPE
    print("\t******************    ORDER    *****************")
    _product = product
    _ORDER_TYPE = type

def releaseOrder(action):
    global _releaseAction
    print("\t*****************    RELEASE    ****************")
    _releaseAction = action


def sendPacket(packet):
    msg = "\t( SENDING  > )"
    msg += " {}".format(" ").join(hex(ord(n)) for n in packet)
    print(msg)
    
    flushSerial()

    try:
        _serialHandler.write(packet)
    except serial.SerialTimeoutException:
        genLog("\t[* ERROR ] SERIAL TIMEOUT\nTrying to reconnect...")
        initSerial()
        return 

def receivePacket():
    global DEBUG, _term_available, _payment_status

    _packet = ""
    _data = []
    _protocol = []
    _payload = ""
    _answer = ""   

    _statusProtocol = False
    _statusTerminal = False
    _statusDevice = False
    _statusTransaction = False

    time.sleep(_comm_period) # Time to receive an answer

    while True:
        if _serialHandler.inWaiting() > 0:
            _packet = _serialHandler.read(1)
            if DEBUG: 
                genLog("\t[ RAW <<<< ] %s" % hex(ord(_packet)))
        else:
            #sendNACK() # no answer
            genLog("\t[* ERROR ] NO ANSWER")
            _answer = "NULL"
            break
        
        _data.append(hex(ord(_packet)))

        # PROTOCOL ----------------------------------------------------
        if _statusProtocol and _packet == __ETX:
            _statusProtocol = False
            genLog("\t(# PROTOCOL <) %s" % ''.join(_protocol))            
            notify("protocol", ''.join(_protocol))
            _protocol = []
            break
            
        elif _statusProtocol:
            _payload += _packet
            _protocol.append(_packet.decode('iso-8859-1'))
            print("\t(# PROTOCOL PIECE <) %s" % hex(ord(_packet)))

        # TERMINAL ----------------------------------------------------
        if _statusTerminal:
            _payload += _packet
            if _packet == __TERM_STATUS_OK:
                _term_available = True

            _packet = ""
            break

        # DEVICE ------------------------------------------------------
        if _statusDevice:
            _statusDevice = False
            _payload += _packet

            if _packet in __DEVICE_STATUS:
                print(": %s" % __DEVICE_STATUS[_packet]) 
            else:
                genLog("\t[* D ERROR ] UNKNOWN %s OPERATION STATUS" % hex(ord(_packet)))

            _answer = _packet            
            _packet = ""
            break

        # TRANSACTION -------------------------------------------------
        if _statusTransaction:
            _statusTransaction = False
            _payload += _packet

            if _packet in __TRANSACTION_STATUS:
                genLog("\t( T ) %s" % __TRANSACTION_STATUS[_packet] )
                _answer = _packet
                if _packet == __APPROVED_PAYMENT:
                    _payment_status = "approved"
                else:
                    _payment_status = "refused"
                break

            _packet = ""            
        # -------------------------------------------------------------


        if _packet in __COMM_STATUS and _packet not in {__STX, __ETX}: 
            if _packet == __STATUS:
                print("\t( C < ) %s" % __COMM_STATUS[_packet]),
            else:
                print("\t( C < ) %s" % __COMM_STATUS[_packet])
            _answer = _packet
            if _packet == __OUTOFORDER:
                __STATE = "init"

        if _packet == __ETX: # END OF PACKET - INTEGRITY CHECK
            CRC = crc16ccitt(_payload) # Computing CRC from received payload
            CRC_MSB = _serialHandler.read(1) # MSB CRC received in Payload
            CRC_LSB = _serialHandler.read(1) # LSB CRC received in Payload
            _data.append(hex(ord(CRC_MSB)))
            _data.append(hex(ord(CRC_LSB)))

            # Compairing CRC computed with received
            if CRC[1] == CRC_MSB and CRC[0] == CRC_LSB :
                genLog("\t[ FAIL ] WRONG CRC  ")
                _flagFail = True # Send NACK

            _payload = ""
            break

        elif _packet == __ACK or _packet == __NACK:
            break

        elif _packet == __STATUS:
            _statusDevice = True
            _payload = _packet

        elif _packet in { __SINGLE_ORDER, __MULT_ORDER}:
            _statusTransaction = True
            _payload = _packet

        elif _packet == __PROTOCOL:
            print("\t [ PROTOCOL ] DETECTED")
            _statusProtocol = True
            _payload = _packet
            _answer = _packet

        elif _packet == __VERIFY_TERM:
            _statusTerminal = True
            _payload = _packet
            print("\t [ TERMINAL ] VERIFYING...")            

        elif _packet == __OUTOFORDER:
            _flagReset = True
            #calibration()
            break

    return _answer

# SERIAL
def initSerial():
    print("\t[ INFO ] CONNECTING TO SERIAL PORT DEVICE: {}".format(_device))
    global _serialHandler
    try:
        _serialHandler = serial.Serial(
            port        = _device,
            baudrate    = _baudrate,
            bytesize    = serial.EIGHTBITS,
            parity      = serial.PARITY_NONE,
            stopbits    = serial.STOPBITS_ONE,
            timeout     = _timeout,
            write_timeout = _delay_write
            )

        if _serialHandler.isOpen():
            print("\t[ INFO ] DEVICE IS ALREADY OPEN")
            _serialHandler.close()
            _serialHandler.open()
        else:
            _serialHandler.open()

    except:
        print("\t[* ERROR ] DEVICE CANNOT BE OPEN")
        sys.exit(1)

def endSerialPort():
    print("\t[ INFO ] DISCONNECTING FROM DEVICE ...")

    if _serialHandler != None:
        if _serialHandler.isOpen():
            _serialHandler.close()

class saleHandler(PatternMatchingEventHandler):
    global _fileHandler, _logFilename
    patterns = ["*.json", "*.dat"]
    

    def process(self, event):
        global _fileHandler, _logFilename
        """
        event.event_type 
            'modified' | 'created' | 'moved' | 'deleted'
        """
        # the file will be processed there
        #print event.src_path, event.event_type  

        with open(event.src_path) as f:
            try:
                data = json.load(f)
                notify = data["notify"]
                print("\t[ INFO ] RECEIVED NOTIFY: %s " % notify)

                _product = {}
                
                # Defining log transaction name
                now = datetime.now()
                _logFilename = _log + str(now.strftime("%m-%d-%Y_%H:%M:%S")) + ".log"
                _fileHandler = open(_logFilename,"a+")

                i = 0
                if notify == "order":
                    print("\t[ INFO ] INITIALIZING ORDER...")
                    for product in data['products']:
                        _product.update({ i: { 'product': product['id'], 'price': product['price']}})
                        genLog("\t[ ORDER ] Product_id: %s, Price: %s"%(str(product['id']), str(product['price'])))
                        i = i + 1
                
                    if i > 1 :
                        startOrder("MULTI", _product)
                    else:
                        startOrder("SINGLE", _product)

                elif notify == "release" and data["action"] in {"CONFIRM", "CANCEL"}:
                    releaseOrder(data["action"])

            except ValueError:
                if DEBUG: print("\t[ ERROR ] Decoding JSON has failed")
    
    def on_modified(self, event):
        self.process(event)

    def on_created(self, event):
        self.process(event)

# LOG GENERATION
def genLog(msg):
    global _fileHandler, _logFilename, _lastMessage
    print(msg)

    if _lastMessage != msg:
        _lastMessage = msg
    else:
        return

    if hasattr(_fileHandler,'closed'):    
        if _fileHandler.closed:
            _fileHandler = open(_logFilename, "a+")

        #print >>_fileHandler,msg  
        _fileHandler.write(str("\n" + msg))
        #_fileHandler.close()  # for testing - should be avoided

def closeLogFile():
    global _fileHandler
    if hasattr(_fileHandler,'closed'):
        #if _fileHandler.closed != False:
        _fileHandler.close()

# COMMANDS

def sendACK(): #UNENCAPSULATED
    flushSerial()
    _serialHandler.write(__ACK)
    print("\t( ACK >) %s"%hex(ord(__ACK)))

def sendNACK(): #UNENCAPSULATED
    flushSerial()
    _serialHandler.write(__NACK)
    print("\t( NACK >) %s"%hex(ord(__NACK)))

def sendENQ(STATUS):
    global _last_packet
    sendPacket( buildPacket(__ENQ) )
    answer = receivePacket()

    _last_packet = answer

    if type(STATUS) is set:
        if answer in STATUS:
            return True
    else:
        if answer == STATUS:
            return True
        elif answer == __NACK:
            return sendENQ(STATUS)
    
    return False 

def sendReset():
    global _RESET 
    print("\n\t[ INIT ] RESETING...")
    sendPacket( buildPacket(__RESET) )
    if receivePacket() == __ACK:
        _RESET = True
        return True

    return False

def sendInitializeDevice():
    print("\t[ INIT ] INITIALIZING DEVICE")
    sendPacket( buildPacket(__MASTER_INIT) )  
    if receivePacket() == __ACK:
        return True
    
    return False
        
def sendEnableDevice():
    print("\t[ INIT ] ENABLING DEVICE") 
    sendPacket( buildPacket(__ENABLE_SLAVE) ) 
    if receivePacket() == __ACK:
        return True
    
    return False

def sendVerifyTerminal():
    global _term_available 
    sendPacket( buildPacket(__VERIFY_TERM) )
    answer = receivePacket()

    if _term_available:
        print("\t[ INFO ] TERMINAL AVAILABLE")
        _term_available = False
        return True
    
    if answer == "NULL":
        print("\t[ WARN ] TERMINAL CHECKING UNAVAILABLE")
        return True

    return False

def sendOrder(type, productId, price):
    selected = __SINGLE_ORDER
    if type == "multiOrder":
        selected = __MULT_ORDER

    sendPacket( buildPacket(selected + chr(productId) + convertToCents(price)) )
    if receivePacket() == __ACK:
        return True

    return False

def sendInitMultiOrder():
    sendPacket( buildPacket(__MULT_ORDER + __MO_INIT) ) 
    
    if receivePacket() == __ACK:
        return True

    return False

def sendEndMultiOrder():
    sendPacket( buildPacket(__MULT_ORDER + __MO_END) )

    if receivePacket() == __ACK:
        return True

    return False

def sendProtocolRequest():
    sendPacket( buildPacket(__PROTOCOL) )
    answer = receivePacket()

    if answer == __PROTOCOL:
        sendACK()
        return True
    elif answer == "NULL": # function not available in protocol version
        print("\t[ WARN ] PROTOCOL UNAVAILABLE")
        return True

    return False

def sendGrantOrder(action): # Confirm or refuse payment
    if action == "CONFIRM":
        sendPacket( buildPacket(__RELEASE + __RELEASE_CONFIRM) )
    else:
        sendPacket( buildPacket(__RELEASE + __RELEASE_REFUSE) )

    if receivePacket() == __ACK:
        return True
    
    return False

    
if __name__ == '__main__':

    print("\t**********************************************")
    print("\t     INITIALIZING VENDPAGO CLIENT DAEMON ")
    print("\t     Version: %s "%VERSION)
    print("\t**********************************************\n")


    # Check if serial port exist
    if os.path.exists(_device) == False:
        print("\t[ ERROR ] Serial port %s not detected" % _device)
        sys.exit(0)

    """
    PRIORITY
    SETTING HIGHER PRIORITY TO THIS SCRIPT
    
    Comm timming is a very important issue in serial communication. 
    Due to this script runs in OS with SOFT TIMMING, it's crucial
    to set a higher nice priority to avoid stalls.
    """
    PID = os.getpid()
    NICE = 10
    output = subprocess.check_output("/usr/bin/renice " + str(NICE) + " -p " + str(PID), shell=True)
    print("\t[ INFO ] SETTING NEW PRIORITY: %s" % output)

    # NOTIFY FOLDER
    if not os.path.exists(_actionDir):
        os.makedirs(_actionDir)

    # TRANSACTION LOG DIR
    if not os.path.exists(_log):
        os.makedirs(_log)    
    
    notify("status", __DISABLED_DEVICE)
    notify("transaction", "none")
    notify("protocol", "none")

    # FILE WATCHER
    observer = Observer()
    observer.schedule(saleHandler(), _actionDir)
    observer.start()

    # INITIALIZING SERIAL
    initSerial()

    # CONCURRENT TASK
    gevent.joinall(
        [
            gevent.spawn(FSM)
        ]
    )

    try:
        observer.join()

        while True:
            time.sleep(1)

    except KeyboardInterrupt:
        print("\t[ INFO ] Ctrl+C pressed. Exiting...")
        observer.stop()

    except (RuntimeError, TypeError, NameError):
        pass

    except:
        print("Unexpected error: {}".format(sys.exc_info()[0]))
        raise

    finally:
        endSerialPort()
        observer.stop()
        sys.exit(0)
